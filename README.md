# Project Name: autoroute-app

## Description
This project is a backend challenge focused on implementing user authentication using social login (Google) in Node.js. It also includes CRUD functionality for managing user details and allows editing user information, except for the email ID, which is not editable. The project uses MySQL as the database and focuses on functional requirements rather than design.

## Getting Started
To get started with the project, follow these steps:

1. Install the project dependencies using npm:
   ```
   npm install
   ```

2. Set up environment variables:
   - Create a `.env` file in the root directory of the project.
   - Add the required environment variables such as database credentials, social login client IDs and secrets, etc.

3. Start the server:
   ```
   npm start
   ```

4. Access the application at [http://localhost:3000/]

5. For fronend Open this
   http://localhost:3000/login.html

## Test Details
The project includes the following test requirements:
- Create a page with a button for social login using Node.js.
- Implement social login authentication (Google/LinkedIn/Facebook) and create a user entry in the database.
- Implement CRUD functionality for managing user details, including editing user information (except for the email ID).
- Validate input fields on the edit page.
- Use MySQL as the database.

## Authors
- Piyush Dabhi
 
Thank you!!!