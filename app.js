
import express from 'express';
import passport from 'passport';
import session from 'express-session';
import sequelize from './config/database.js';
import routes from './routes/index.js';
import './config/passport.js';
import morgan from 'morgan';
import { fileURLToPath } from 'url'; 
import path from 'path';
const __filename = fileURLToPath(import.meta.url); // Get the current file's path
const __dirname = path.dirname(__filename); // Get the directory name

const app = express();
 
// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(session({  secret: process.env.SESSION_SECRET , resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session()); 
app.use(morgan('dev'))

// Serve static files (including the login page)
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.use('/api', routes);

// Start server
const PORT = process.env.PORT || 3000;
sequelize.sync().then(() => {
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
  });
});
