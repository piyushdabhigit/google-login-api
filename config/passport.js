// config/passport.js
import dotenv from 'dotenv';
import passport from 'passport';
import { Strategy as GoogleStrategy } from 'passport-google-oauth20';
import User from '../models/user.js';

dotenv.config();

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  try {
    const user = await User.findByPk(id);
    done(null, user);
  } catch (error) {
    done(error, null);
  }
});

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: '/api/auth/google/callback'
},
async (accessToken, refreshToken, profile, done) => {
  try {
    let user = await User.findOne({ where: { googleId: profile.id } });
    if (!user) {

      // Create new user if not found
      user = await User.create({
        googleId: profile.id,
        displayName: profile.displayName,
        email: profile.emails && profile.emails[0] && profile.emails[0].value,
        profilePicture: profile.photos && profile.photos[0] && profile.photos[0].value,
        // Add any other fields you want to populate from the Google profile
      });
    }


    done(null, user);
  } catch (error) {
    console.error('Error in Google strategy:', error);
    done(error, null); // Pass the error to the done callback
  }
}));

export default passport;
