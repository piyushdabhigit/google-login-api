
import express from 'express';
import userRoutes from './userRoutes.js'; 
import authRoutes from './authRoutes.js'; 

const router = express.Router();

router.use(userRoutes);
router.use(authRoutes);

export default router;
