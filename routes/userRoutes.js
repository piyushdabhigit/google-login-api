
import express from 'express';
import { getUsers, updateUser, deleteUser,createUser } from '../controllers/userController.js';
import { body, param, query } from 'express-validator';

const router = express.Router();

// Define validation rules for create user endpoint
const createUserValidationRules = [
  body('displayName').notEmpty().withMessage('Display name is required'),
  body('email').isEmail().withMessage('Invalid email address'),
  body('profilePicture').optional().isURL().withMessage('Invalid profile picture URL')
];

// POST /api/users
router.post('/users', createUserValidationRules, createUser);

// Define validation rules for get users endpoint
const getUsersValidationRules = [
  query('page').optional().isInt({ min: 1 }).withMessage('Page must be a positive integer'),
  query('limit').optional().isInt({ min: 1 }).withMessage('Limit must be a positive integer'),
  query('search').optional().isString().withMessage('Search query must be a string')
];

// GET /api/users
router.get('/users', getUsersValidationRules, getUsers);

// Define validation rules for update user endpoint
const updateUserValidationRules = [
  param('id').notEmpty().withMessage('User ID is required'),
  body('displayName').optional().isString().withMessage('Display name must be a string'),
  body('profilePicture').optional().isURL().withMessage('Profile picture must be a valid URL'),
];

// PUT /api/users/:id
router.put('/users/:id', updateUserValidationRules, updateUser);

// Define validation rules for delete user endpoint
const deleteUserValidationRules = [
  param('id').notEmpty().withMessage('User ID is required')
];

// DELETE /api/users/:id
router.delete('/users/:id', deleteUserValidationRules, deleteUser);

export default router;
