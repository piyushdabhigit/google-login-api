
import express from 'express';
import { googleAuth, googleAuthCallback } from '../controllers/authController.js';
const router = express.Router();

router.get('/auth/google', googleAuth);
router.get('/auth/google/callback', googleAuthCallback);

export default router;
