import { DataTypes } from 'sequelize';
import sequelize from '../config/database.js';

const User = sequelize.define('User', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false
  },
  googleId: {
    type: DataTypes.STRING,
    allowNull: true,
    unique: true
  },
  displayName: {
    type: DataTypes.STRING,
    allowNull: true
  },
  email: {
    type: DataTypes.STRING,
    allowNull: true,
    unique: true,
    validate: {
      isEmail: true
    }
  },
  profilePicture: {
    type: DataTypes.STRING,
    allowNull: true
  },
  isDelete: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false // Default value set to false
  }
}, {
  timestamps: true
});
export default User;
