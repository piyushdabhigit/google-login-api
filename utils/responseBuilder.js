//common response

export const successResponse = (res, responseCode = 200, msg, data = {}) => {
    return res.status(responseCode).json({
        status: 1,
        message: msg,
        data: data,
        currentTime: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
    });
};

export const errorResponse = (res, responseCode, msg, data = {}) => {
    return res.status(responseCode).json({
        status: 0,
        message: msg,
        data: data
    });
};
