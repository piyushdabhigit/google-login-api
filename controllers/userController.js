import { validationResult } from 'express-validator';
import User from '../models/user.js';
import { successResponse, errorResponse } from '../utils/responseBuilder.js';
import { Op } from 'sequelize';

export const getUsers = async (req, res) => {
  const { page = 1, limit = 10, search } = req.query;
  const offset = (page - 1) * limit;
  
  try {
    let whereClause = { isDelete: false };
    if (search) {
      // Add search condition if search query is provided
      whereClause = {
        ...whereClause,
        [Op.or]: [
          { displayName: { [Op.like]: `%${search}%` } },
          { email: { [Op.like]: `%${search}%` } }
        ]
      };
    }

    const { count, rows: users } = await User.findAndCountAll({
      where: whereClause,
      limit: parseInt(limit),
      offset: offset
    });

    if (!users || users.length === 0) {
      return errorResponse(res, 404, 'No users found');
    }

    return successResponse(res, 200, 'List of users fetched successfully', {
      users,
      totalUsers: count
    });
  } catch (error) {
    console.error('Error fetching users:', error);
    return errorResponse(res, 500, 'Internal server error', error.message);
  }
};

export const updateUser = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) { 
      return errorResponse(res, 400, 'Validation error', errors.array()); 
    }

    // Extract user ID from request parameters
    const userId = req.params.id;

    // Check if the user exists
    const user = await User.findByPk(userId);
    if (!user) {
      return errorResponse(res, 404, 'User not found');
    } 
    // Extract update fields from the request body
    const { displayName, profilePicture } = req.body;
    
    // Update user attributes if provided
    if (displayName !== undefined) user.displayName = displayName;
    if (profilePicture !== undefined) user.profilePicture = profilePicture;

    // Save the updated user
    await user.save();

    // Fetch the updated user data after save
    const updatedUser = await User.findByPk(userId);

    // Return success response with updated user data
    return successResponse(res, 200, 'User updated successfully', updatedUser);
  } catch (error) {
    // Handle any errors that occur during the update process
    console.error('Error updating user:', error);
    return errorResponse(res, 500, 'Internal server error', error.message);
  }
};

export const deleteUser = async (req, res) => {
  const userId = req.params.id;
  try {
    // Find user by ID
    const user = await User.findByPk(userId);

    if (!user) {
      return errorResponse(res, 404, 'User not found');
    }

    // Soft delete the user by setting the isDelete flag to true
    user.isDelete = true;
    await user.save();

    return successResponse(res, 200, 'User deleted successfully', user);
  } catch (error) {
    console.error('Error deleting user:', error);
    return errorResponse(res, 500, 'Internal server error', error.message);
  }
};

export const createUser = async (req, res) => {
  try {
    // Check for validation errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) { 
      return errorResponse(res, 400, 'Validation error', errors.array()); 
    }

    // Extract user data from request body
    const { displayName, email, profilePicture } = req.body;

    // Check if email already exists
    const existingUser = await User.findOne({ where: { email } });
    if (existingUser) {
      return errorResponse(res, 400, 'Email already exists');
    }

    // Create new user instance
    const newUser = await User.create({
      displayName,
      email,
      profilePicture
    });

    // Return success response with created user data
    return successResponse(res, 201, 'User created successfully', newUser);
  } catch (error) {
    // Handle any errors that occur during user creation
    console.error('Error creating user:', error);
    return errorResponse(res, 500, 'Internal server error', error.message);
  }
};