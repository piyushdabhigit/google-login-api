import passport from 'passport';

// Redirect to Google OAuth authentication
export const googleAuth = passport.authenticate('google', { scope: ['profile', 'email'] });

// Google OAuth callback
export const googleAuthCallback = (req, res) => {
  passport.authenticate('google', { failureRedirect: '/login' })(req, res, () => {
    // Redirect to user list page after successful login
    res.redirect('/user-list.html');
  });
};

